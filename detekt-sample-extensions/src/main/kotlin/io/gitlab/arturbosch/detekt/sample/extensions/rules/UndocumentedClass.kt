package io.gitlab.arturbosch.detekt.sample.extensions.rules

import io.gitlab.arturbosch.detekt.api.CodeSmell
import io.gitlab.arturbosch.detekt.api.Config
import io.gitlab.arturbosch.detekt.api.Debt
import io.gitlab.arturbosch.detekt.api.Entity
import io.gitlab.arturbosch.detekt.api.Issue
import io.gitlab.arturbosch.detekt.api.Rule
import io.gitlab.arturbosch.detekt.api.Severity
import org.jetbrains.kotlin.psi.KtClass
import org.jetbrains.kotlin.psi.KtClassOrObject
import org.jetbrains.kotlin.psi.KtObjectDeclaration
import org.jetbrains.kotlin.psi.psiUtil.isPublic

class UndocumentedClass(config: Config) : Rule(config) {

	override val issue = Issue(javaClass.simpleName, Severity.Maintainability, "Missing documentation.", Debt.TEN_MINS)

	private val searchPatterns = valueOrDefault(SEARCH_PATTERNS, emptyList<String>()).toSet().map { Regex(it) }
	private val patternsToIgnore = valueOrDefault(PATTERNS_TO_IGNORE, emptyList<String>()).toSet().map { Regex(it) }

	override fun visitClass(klass: KtClass) {
		val path = Entity.from(klass.containingKtFile).location.file
		if (isValidPath(path) && requiresDocumentation(klass)) {
			reportIfUndocumented(klass)

			val constructorParameters = klass.primaryConstructorParameters
			val classDocumentation = klass.docComment?.text ?: ""

			constructorParameters.forEach {
				if (it.isPublic && !classDocumentation.contains("@property ${it.name} ")) {
					report(CodeSmell(issue, Entity.from(it),
						"${it.name} is not documented. Add '@property ${it.name} your desc' to ${klass.nameAsSafeName}"))
				}

				if (it.docComment != null) {
					report(CodeSmell(issue, Entity.from(it),
						"${it.name} should NOT be documented inside constructor."))
				}
			}
		}

		super.visitClass(klass)
	}

	private fun requiresDocumentation(klass: KtClass): Boolean {
		return ((klass.isPublic || klass.isTopLevel() || klass.isInnerClass() || klass.isNestedClass() || klass.isInnerInterface()))
			&& !klass.isInternal()
	}

	override fun visitObjectDeclaration(declaration: KtObjectDeclaration) {
		val path = Entity.from(declaration.containingKtFile).location.file
		if (isValidPath(path) && !declaration.isCompanionWithoutName() && !declaration.isLocal) {
			reportIfUndocumented(declaration)
		}

		super.visitObjectDeclaration(declaration)

	}

	private fun reportIfUndocumented(element: KtClassOrObject) {
		if (element.notEnumEntry() && element.docComment == null) {
			report(CodeSmell(issue, Entity.from(element),
				"${element.nameAsSafeName} is missing required documentation."))
		}
	}

	private fun isValidPath(path: String): Boolean {
		return searchPatterns.any { it.matches(path) } && !patternsToIgnore.any { it.matches(path) }
	}

	companion object {
		const val SEARCH_PATTERNS = "searchPatterns"
		const val PATTERNS_TO_IGNORE = "patternsToIgnore"
	}
}
