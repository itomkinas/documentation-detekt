package io.gitlab.arturbosch.detekt.sample.extensions.rules

data class InfoStatus(
	val hello: String,
	val undocumentedClass: UndocumentedClass
)


class HelloStatus()

internal data class InternalStatus(val helloworld: Int)
