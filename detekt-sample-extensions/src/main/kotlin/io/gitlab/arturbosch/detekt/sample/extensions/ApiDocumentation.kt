package io.gitlab.arturbosch.detekt.sample.extensions

import io.gitlab.arturbosch.detekt.api.Config
import io.gitlab.arturbosch.detekt.api.RuleSet
import io.gitlab.arturbosch.detekt.api.RuleSetProvider
import io.gitlab.arturbosch.detekt.sample.extensions.rules.UndocumentedClass

class ApiDocumentation : RuleSetProvider {

	override val ruleSetId: String = "api-documentation"

	override fun instance(config: Config): RuleSet {
		return RuleSet(ruleSetId, listOf(
				UndocumentedClass(config)
		))
	}
}
