package io.gitlab.arturbosch.detekt.sample.extensions.rules

import io.gitlab.arturbosch.detekt.api.Entity
import org.jetbrains.kotlin.lexer.KtTokens
import org.jetbrains.kotlin.psi.KtClass
import org.jetbrains.kotlin.psi.KtClassOrObject
import org.jetbrains.kotlin.psi.KtEnumEntry
import org.jetbrains.kotlin.psi.KtObjectDeclaration

internal fun KtObjectDeclaration.isCompanionWithoutName() =
	isCompanion() && nameAsSafeName.asString() == "Companion"

internal fun KtClass.isNestedClass() = !isInterface() && !isTopLevel() && !isInner()

internal fun KtClass.isInnerClass() = !isInterface() && isInner()

internal fun KtClass.isInnerInterface() = !isTopLevel() && isInterface()

internal fun KtClassOrObject.notEnumEntry() = this::class != KtEnumEntry::class

internal fun KtClass.isInternal(): Boolean {
	this.modifierList?.let {
		val abstractModifier = it.getModifier(KtTokens.INTERNAL_KEYWORD)
		if (abstractModifier != null) {
			return true
		}
	}

	return false
}
